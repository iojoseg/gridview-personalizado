package mx.tecnm.misantla.myappgridviewp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.GridView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var paises = ArrayList<Paises>()

       /* paises.add("Mexico")
        paises.add("USA")
        paises.add("Honduras")
        paises.add("Argentina")*/

        paises.add(Paises("Mexico",R.drawable.mexico))
        paises.add(Paises("USA",R.drawable.usa))
        paises.add(Paises("Argentina",R.drawable.argentina))
        paises.add(Paises("Honduras",R.drawable.honduras))



        var grid: GridView = findViewById(R.id.Grid)
       // val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,paises)
        val adaptador = AdaptadorCustom(this,paises)
        grid.adapter = adaptador

        grid.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            Toast.makeText(this,paises.get(position).nombre, Toast.LENGTH_LONG).show()
        }

    }
}